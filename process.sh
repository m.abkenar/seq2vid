#!/bin/bash
#seq2vid : Compose a video out of a text sequence
#Copyright (C) 2014-2017 Masoud Abkenar

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

NFRAME=$(( frame ))		# total number of segments added, including those from the template

echo ""
# The following loop goes over all segments both provided by the user and also defined in the template.
# For each segment, two videos are made: the main segment video (slide???_0.avi)
# and the fading video to the next segment (slide???_1.avi).
for (( i=0; i < NFRAME; i++ )); 
{
	iii=`printf '%03d' $i`		# This converts '1' to '001' and so on.
	slidefile=slide${iii}${imgsuffix}

	if [ ${st[$i]} = "-video" ]	# if the segment type is 'video'
	then
		echo "Processing video file '"${vf[$i]}"'..."
		# We convert the input video to the native codec of this program
		# in the specified time range from start_time until end_time,
		# and add necessary padding so that it matches the output aspect ratio.
		$ffmpeg -y -ss ${StartTime[i]} -t "$( echo "scale=1;${EndTime[i]}-${StartTime[i]}" | bc )" -i "${vf[$i]}" -r ${FPS} -an -c:v mjpeg -filter:v "scale=iw*min(${videoX}/iw\,${videoY}/ih):ih*min(${videoX}/iw\,${videoY}/ih), pad=${videoX}:${videoY}:(${videoX}-iw*min(${videoX}/iw\,${videoY}/ih))/2:(${videoY}-ih*min(${videoX}/iw\,${videoY}/ih))/2:${bg_color}" ${slidefile%.*}_0.avi >/dev/null 2>&1

		# We need to extract last frame of the video as a static image for fading to the next segment.
		video_last_frame "${slidefile%.*}_0.avi" "${slidefile}"	# taking last frame directly from the converted+padded video

	else	# else if the segment type is any of title, text, or image
		make_static_video slide${iii}${imgsuffix} ${ts[$i]}
	fi
	# main video of this segment made. Now we make the fading effect to the next segment:
	i_plus_one=$(( i + 1 ))
	iii_plus_one=`printf '%03d' ${i_plus_one} `
	if [ ${TransitionStyle[i]} = "morph" ]
	then
		make_morphed_video slide${iii}${imgsuffix} slide${iii_plus_one}${imgsuffix} tt[$i] slide${iii}_1.avi
	elif [ ${TransitionStyle[i]} = "fade" ]
	then
		make_morphed_video slide${iii}${imgsuffix} "${slide_template}" tt[$i]/2 slide${iii}_1.avi
		make_morphed_video "${slide_template}" slide${iii_plus_one}${imgsuffix} tt[$i]/2 slide${iii}_2.avi
	else
		echo "Unknown transition style defined in sequence or template file. Options are: 'morph', 'fade'. Aborting."
		echo "TransitionStyle["$i"] is "${TransitionStyle[i]}
		echo ""
		exit 1
	fi
}
## Now we merge all videos we have made in the above loop to get the final video output.
echo "Rendering the final video output '"${output}"'..."
# Removing any previously created output video:
if [ -e "${WorkingPath}${PathSep}${output}" ]; then rm "${WorkingPath}${PathSep}${output}";fi

## METHOD 1, ffmpeg: We make a long string for the list of all temporary video files that we should pass as arguments of ffmpeg command:
ffmpeg_arg=""

#for (( j=0; j < $NFRAME; j++ )); do jjj=`printf '%03d' ${j} `; ffmpeg_arg=${ffmpeg_arg}slide${jjj}_0.avi\\\|slide${jjj}_1.avi\\\|;done
for j in slide???_?.avi; do ffmpeg_arg=${ffmpeg_arg}${j}\\\|; done

## Last touch on the list of files:
ffmpeg_arg=${ffmpeg_arg%\\*} 
## Making the full command:
## This is how it looks like (human readable) for example: ffmpeg -i concat:slide000_0.avi\|slide000_1.avi\|slide001_0.avi\|slide001_1.avi -c copy demo.avi
ffmpeg_command="$ffmpeg -i concat:${ffmpeg_arg} -an -c:v $codec '${WorkingPath}${PathSep}${output}'>/dev/null 2>&1"
## Running the command to join all the temporary videos together for the final video:
eval "${ffmpeg_command}"

### METHOD 2, mencoder:
#$mencoder -really-quiet -ovc x264 -oac copy slide???_?.avi -o "${output}" >/dev/null 2>&1

echo ""
echo "Rendering finished. The output file is:"
ls -sh "${WorkingPath}${PathSep}${output}"

rm slide???_?.avi
rm slide???${imgsuffix}
rm ${slide_template}
rm template_constants.sh
rm template_post.sh
rm template_pre.sh
