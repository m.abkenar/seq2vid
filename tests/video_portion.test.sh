#!/bin/bash
name="video_portion.plain"

echo "Performing test '"$name"'"

rm -f video_portion.mp4
../seq2vid video_portion.seq >/dev/null 2>&1

NewChecksum=`md5sum video_portion.mp4 | awk '{print $1}'`
OldChecksum=$(getChecksum $name)

if [ "$NewChecksum" == "$OldChecksum" ]
then
	successCount=$(( successCount + 1 ))
	echo PASSED
else
	failCount=$(( failCount + 1 ))
	echo FAILED
fi
rm video_portion.mp4
