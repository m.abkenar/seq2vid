#!/bin/bash
name="demo.plain"

echo "Performing test '"$name"'"

rm -f demo.mp4
../seq2vid >/dev/null 2>&1

NewChecksum=`md5sum demo.mp4 | awk '{print $1}'`
OldChecksum=$(getChecksum $name)

if [ "$NewChecksum" == "$OldChecksum" ]
then
	successCount=$(( successCount + 1 ))
	echo PASSED
else
	failCount=$(( failCount + 1 ))
	echo FAILED
fi
rm demo.mp4
