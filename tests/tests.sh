checksumDB="checksums.md5"
function getChecksum {
	local name="$1"
	echo  `grep "$name" $checksumDB | awk '{print $1}'`
}

successCount=0
failCount=0

. demo.plain.test.sh
. demo.FZJ-ICS2.test.sh
. BirdFlocking.plain.test.sh
. end-to-end-encryption.plain.test.sh
. best-memories.summer.test.sh
. video_portion.test.sh

echo 
echo "Tests finished. "$successCount" tests passed, while "$failCount" tests failed."
echo
